import time
import allure
from selenium import webdriver
from selenium.webdriver.common.by import By

class Test():
    #前置动作
    def setup_method(self, method):
        #实例化driver
        self.driver = webdriver.Chrome()
        #隐式等待
        self.driver.implicitly_wait(5)

    # 后置动作
    def teardown_method(self, method):
        self.driver.quit()

    #搜索霍格沃兹测试开发
    def test_(self):
        with allure.step("打开网址"):
          self.driver.get("https://ceshiren.com/")
          time.sleep(3)
        with allure.step("调整窗口大小"):
          self.driver.set_window_size(1051, 798)

        with allure.step("点击输入框"):
           self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        with allure.step("点击后输入关键词"):
           self.driver.find_element(By.ID, "search-term").send_keys("web自动化")
           time.sleep(3)
        with allure.step("断言测试"):
           self.driver.find_element(By.CSS_SELECTOR, ".widget-link > .discourse-tag").click()
           self.driver.find_element(By.LINK_TEXT, "web测试小技巧之冻结窗口").click()
           assert self.driver.find_element(By.LINK_TEXT, "web测试小技巧之冻结窗口").text == "web测试小技巧之冻结窗口"


#pytest -vs test_web_project.py/ --alluredir=./results
#allure serve  ./results

