import pytest
def add(a, b):
    return a + b

# 被测函数
def add(a, b):
    return a + b
#pytest前后置动作
def  setup_function( ):
    print("开始计算")   # 注意缩进
def teardown_function( ):
    print("计算结束")    # 注意缩进
def teardown():
 print("---结束测试---")

@pytest.mark.hebeu
def test1():
    assert add(1, 6) == 7
    assert add(-99, 0) == -99

@pytest.mark.hebeu
def test2():
    assert add(1.1, 2.2) == pytest.approx(3.3, 0.01)

@pytest.mark.hebeu
def test3():
    assert add(99.99, -99.99) == pytest.approx(0, 0.01)
    assert add(50, -50) == 0